<?php

/**
 * @file    RendFilter.inc
 * @brief   Collects array index paths based on select strings similar to CSS
 *          selectors.
 * @author  Joon Park (joon@dvessel.com)
 *
 * This is specific to Drupal Render Arrays. Called from RendElements.inc.
 */

/**
 *
 */
class RendFilter extends RecursiveFilterIterator {

  private $it;
  private $parent;
  private $depth      = 0;
  private $row        = 0;
  private $row_count  = 0;
  private $path_index = array();

  private $queries;
  private $query;
  private $q;
  private $skip_siblings;
  private $skip_children;
  private $matches = array();
  protected $found = array();

  /**
   * Constructs a filter around another iterator.
   *
   * @param it
   *  Iterator to filter
   */
  public function __construct(RecursiveArrayIterator $it) {
    $this->it = $it;
    parent::__construct($it);
  }

  /**
   * Prepare a search query.
   *
   * @return
   *  Whether it is valid or not.
   */
  public function search() {
    if (isset($this->queries)) {
      $this->depth         = 0;
      $this->row           = 0;
      $this->path_index    = array();
      $this->query = $this->queries->current();
      $this->q             = 0;
      $this->skip_siblings = FALSE;
      $this->skip_children = FALSE;
      $this->matches       = array();
      $this->found         = array();
      return $this->queries->valid();
    }
    else {
      return FALSE;
    }
  }

  /**
   * Increment to the next query set.
   */
  public function searchNext() {
    $this->queries->next();
  }

  /**
   * @param $search_string
   *  A select string. This is required before the iteration starts.
   */
   public function selection($search_string) {
     $queries = $this->parseSelectors($search_string);
     $queries = $this->hintQueries($queries);
     $this->queries = new ArrayIterator($queries);
   }

  /**
   * Parse selector string.
   *
   * @param $select_string
   *  CSS like select string.
   * @return
   *  Array representation of the select string.
   */
  protected function parseSelectors($select_string) {

    // elements
    $e_regex = "(\*)?
                ([[:alnum:]-_]+)?
                (?>\#([[:alpha:]][[:alnum:]_-]+))?
                (?>\.([[:alpha:]][[:alnum:]_-]+))?
                (?>\[\s*([[:alnum:]-_]+)\s*(?>(?>   =\s*['\"]?([^'\"]+?)['\"]?)
                                            | (?> \^=\s*['\"]?([^'\"]+?)['\"]?)
                                            | (?> \*=\s*['\"]?([^'\"]+?)['\"]?)
                                            | (?> \~=\s*['\"]?([^'\"]+?)['\"]?)
                                            | (?>\\$=\s*['\"]?([^'\"]+?)['\"]?)
                | (?>\[\s*([[:alnum:]_-]+?)\s*\]\s*=?\s*['\"]?([^'\"]+?)?['\"]?))?\s*\])?";
    // pseudo nth child arguments
    $n_regex = "(?(?<=nth-child|nth-last-child) \(\s* (?>\s*(?>([+-]?[[:digit:]]*)[Nn])? \s*([+-]?[[:digit:]]*)? | \s*(odd|even) \s*) \s*\) )?";
    // lone pseudo and pseudo nth child plus negation of all preceding elements.
    $p_regex = "(?>:([[:alpha:]-]+) $n_regex (?(?<=not) \(\s* (?> $e_regex | :([[:alpha:]-]+) $n_regex ) \s*\) )?)?";
    // full
    $full_regex = "/(?>^|(\s*\+\s*)|(\s*\~\s*)|(\s*\>\s*)|(\s+)) $e_regex $p_regex/x";

    $groups = array();
    foreach (explode(',', $select_string) as $selection) {
      $selection_match = array();
      preg_match_all($full_regex, trim($selection), $selection_match, PREG_SET_ORDER | PREG_OFFSET_CAPTURE);
      $groups[] = $selection_match;
    }

    // positioning lookup.
    $lookup = array(
       0 => 'query',
       1 => 'adjacent_sibling',
       2 => 'general_sibling',
       3 => 'immediate_descendent',
       4 => 'descendent',
       5 => 'universal',
       6 => 'key',
       7 => 'id',
       8 => 'class',
       9 => 'attribute',
      10 => 'equals',
      11 => 'begins',
      12 => 'contains',
      13 => 'space_separated',
      14 => 'ends',
      15 => 'sub_key',
      16 => 'sub_equals',
      17 => 'pseudo',
      18 => 'nth_a',
      19 => 'nth_b',
      20 => 'nth_keyword',
      // negated items.
      21 => 'universal',
      22 => 'key',
      23 => 'id',
      24 => 'class',
      25 => 'attribute',
      26 => 'equals',
      27 => 'begins',
      28 => 'contains',
      29 => 'space_separated',
      30 => 'ends',
      31 => 'sub_key',
      32 => 'sub_equals',
      33 => 'pseudo',
      34 => 'nth_a',
      35 => 'nth_b',
      36 => 'nth_keyword',
    );

    $queries = array();
    foreach ($groups as $selection_match) {
      $c = 0;
      $total = count($selection_match);
      $query = array();
      foreach ($selection_match as $match) {
        $item = new stdClass();
        $item->query    = $match[0][0];
        $item->position = $c++;
        $item->final    = $c === $total;

        foreach ($match as $i => $select) {
          if ($select[1] === -1 || $select[0] === 'not') { continue; }

          $name    = $lookup[$i];
          $value   = $select[0];
          $negated = $i >= 21 && $i <= 36;

          // Delimiters.
          if ($i >= 1 && $i <= 4) {
            $item->$name = TRUE;
          }
          // universal, keys and id's.
          elseif ($i >= 5 && $i <= 7) {
            $item->$name = $value;
          }
          // classes.
          elseif ($i === 8) {
            $item->class[] = $value;
          }
          // attribute and values.
          elseif ($i >=  9 && $i <= 16 || $i >= 25 && $i <= 32) {
            if ($name === 'attribute') {
              $attr = new stdClass();
              $attr->get     = 'name';
              $attr->negated = $negated;
              $attr->name = $value;
              $item->attribute[] = $attr;
            }
            else {
              $attr->get   = $name;
              $attr->$name = $value;
            }
          }
          // pseudo and arguments.
          elseif ($i >=  17 && $i <= 36) {
            if ($name === 'pseudo') {
              $item->pseudo[$value] = !$negated;
              if (in_array($value, array('nth-child', 'nth-last-child'))) {
                $args = new stdClass();
                $args->negated = $negated;
                $args->type    = $value;
                $item->nth_child_args = $args;
              }
            }
            // nth child arguments.
            elseif ($i >= 18 && $i <= 20 || $i >= 34 && $i <= 36) {
              $args->$name = $value;
            }
            // negated universal, keys and id's.
            elseif ($i >= 21 && $i <= 23) {
              $item->pseudo["!$name"] = $value;
            }
            // negated classes.
            elseif ($i === 24) {
              $item->pseudo["!$name"][] = $value;
            }
          }
        }

        $query[] = $item;
      }
      $queries[] = $query;
    }

    return $queries;
  }

  /**
   * Hint queries for faster performance.
   * 
   * TODO: this is ugly and can be improved.
   *
   * @param $queries
   *  The parsed selection string
   * @return
   *  Hinted queries.
   */
  protected function hintQueries($queries) {

    foreach ($queries as $query) {
      $is_unique = FALSE;
      foreach ($query as $i => $item) {
        if (!$item->final) {
          $next_item = $query[$i + 1];
        }
        else {
          $next_item = new stdClass();
        }

        $has_key  = !empty($item->key);
        $has_id   = !empty($item->id);
        $has_root = !empty($item->pseudo['root']);
        $has_desc = !empty($item->descendent);
        $has_imde = !empty($item->immediate_descendent);
        $has_gsib = !empty($item->general_sibling);
        $has_asib = !empty($item->adjacent_sibling);
        $has_ngsi = !empty($next_item->general_sibling);
        $has_nasi = !empty($next_item->adjacent_sibling);

        if ($has_key || $has_id || $has_root) {
          $is_unique = TRUE;
        }
        elseif ($has_desc || $has_gsib) {
          $is_unique = FALSE;
        }

        if ($is_unique) {
          if ($i === 0 && empty($next_item) || $has_imde || $has_gsib || $has_asib) {
            // When a match fails, it's a waste of cpu time to continue searching
            // through children. Flag it here so there's less overhead in checking
            // through the iterator.
            $item->on_miss['skip child'] = TRUE;
          }
          if ($item->final || $has_ngsi || $has_nasi) {
            // Look ahead for sibling selectors and skip children for the current
            // element if the current element is unique. This also applies for
            // final matches.
            $item->on_match['skip child'] = TRUE;
          }
          if ((($has_key || $has_id) || $has_asib) && !$has_nasi && !$has_ngsi) {
            // Check for concurrently chained adjacent sibling selectors and skip
            // siblings when the next item is not another sibling selector. Also
            // looks for keys and id's.
            $item->on_match['skip siblings'] = TRUE;
          }
          // Skip all siblings of all parents if it's the first query looking for
          // a key or id.
          if (($has_key || $has_id) && $i === 0) {
            $item->on_match['skip uncles'] = TRUE;
          }
        }
        // Normally the pointer increments on each depth. Sibling selectors
        // must increment from row.
        if ($has_ngsi || $has_nasi) {
          $item->on_match['increment from row'] = TRUE;
        }
      }
    }

    return $queries;
  }

  /**
   * Accept function to decide whether an element of the inner iterator
   * should be accessible through the Filteriterator.
   *
   * @return
   *  Whether or not to expose the current element of the inner iterator.
   */
  public function accept() {

    // Skip attributes and non-arrays. Skip flag also checked here.
    $index = $this->key();
    if ($index[0] === '#' ||  $this->skip_siblings ||  !is_array($this->current())) {
      return FALSE;
    }

    // Build path data and query matches.
    $this->path_index[$this->depth] = $index;
    ++$this->row;

    // Do not write to this query! To be cloned on match.
    $query = $this->query[$this->q];

    if ($this->matchCurrent($query)) {
      if (!empty($query->on_match['skip child'])) {
        // The follow skips children when there is a match:
        // - when the next query is a sibling selector.
        // - when it's the final query and the match is unique.
        $this->skip_children = TRUE;
      }
      if (!empty($query->on_match['skip siblings'])) {
        // Skips siblings when there is a unique match except when the
        // next query is a sibling selector.
        $this->skip_siblings = TRUE;
      }
      if (!empty($query->on_match['skip uncles'])) {
        // Skips siblings of parents on the first unique match.
        $instance = $this;
        while ($instance = $instance->parent) {
          $instance->skip_siblings = TRUE;
        }
      }
      // TODO: this needs better handling. Misses on non-unique selectors.
      // increment the query pointer for sibling selectors.
      if (!empty($query->on_match['increment from row'])) {
        $this->q = $query->position + 1;
      }
    }
    else {
      if (!empty($query->on_miss['skip child']) || !empty($query->key) && $this->it->offsetExists($query->key)) {
        // The following skips children on miss.
        // - :root
        // - immediate descendent
        // - general sibling
        // - adjacent sibling
        //
        // Also skip when searching for a key and it's detected within the current depth.
        $this->skip_children = TRUE;
      }
    }

    return TRUE;
  }

  /**
   * When the current element has passed accept() and contains children,
   * further checks can be made here to allow or prevent further processing of
   * child elements. Do not call directly.
   */
  public function acceptChildren() {
    if (!empty($this->skip_children)) {
      // Reset flag.
      $this->skip_children = FALSE;
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Filters a selection from a query against the current item being iterated.
   */
  protected function matchCurrent($query) {

    // Main filters.
    if (isset($query->key)       && $query->key !== $this->key()
    ||  isset($query->id)        && $query->id  !== $this->getAttributeId()
    ||  isset($query->class)     && array_diff($query->class, $this->getAttributeClass())
    ||  isset($query->attribute) && !$this->matchAttributes($query)
    ||  isset($query->pseudo)    && !$this->matchPseudo($query)
    || !empty($this->matches)    && !$this->matchRelations($query, $this->matches)) {
      return FALSE;
    }

    // Add match.
    $this->matches[$this->depth][$this->row] = clone $query;
    // Collect found.
    if ($query->final) {
      $this->found[] = $this->path_index;
    }

    return TRUE;
  }

  /**
   * Match attribute selectors. Follows convention set from W3C plus a few
   * custom shortcuts.
   *
   * @see http://www.w3.org/TR/css3-selectors/#attribute-selectors
   */
  protected function matchAttributes($query) {

    foreach ($query->attribute as $attr) {
      if ($attribute = $this->getAttribute($attr->name)) {
        if ($attr->get !== 'name') {
          // Look for value.
          if (is_array($attribute) || is_object($attribute)) {
            $attribute_array = array();
            foreach ((array) $attribute as $k => $v) {
              $attribute_array[$k] = is_bool($v) ? $v ? 'true' : 'false' : (string) $v;
            }
            if (!empty($attr->sub_key)
            && (!isset($attribute_array[$attr->sub_key])
            ||  !empty($attr->sub_equals) && $attribute_array[$attr->sub_key] !== $attr->sub_equals)) {
              return FALSE;
            }
            $attribute = implode(' ', $attribute_array);
          } 
          elseif (is_bool($attribute)) {
            $attribute = $attribute ? 'true' : 'false';
          }
          elseif (!is_string($attribute)) {
            $attribute = (string) $attribute;
          }
          switch ($attr->get) {
            case 'equals':   if ($attr->equals !== $attribute)                           { return FALSE; } break;
            case 'begins':   if (strpos($attribute, $attr->begins) !== 0)                { return FALSE; } break;
            case 'contains': if (strpos($attribute, $attr->contains) === FALSE)          { return FALSE; } break;
            case 'ends':     if (strpos(strrev($attribute), strrev($attr->ends)) !== 0)  { return FALSE; } break;
            case 'space_separated':
              if (!in_array($attr->space_separated, explode($attribute)))                { return FALSE; } break;
          }
        }
      }
      else {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Match pseudo selectors. Follows convention set from W3C plus a few custom
   * shortcuts.
   *
   * @see http://www.w3.org/TR/css3-selectors/#pseudo-classes
   */
  protected function matchPseudo($query) {

    if (isset($query->pseudo['root'])
        && ($query->pseudo['root'] && $this->depth !== 0
        || !$query->pseudo['root'] && $this->depth === 0)
    ||  isset($query->pseudo['empty'])
        && ($query->pseudo['empty'] && $this->hasContent()
        || !$query->pseudo['empty'] && !$this->hasContent())
    ||  isset($query->pseudo['first-child'])
        && ($query->pseudo['first-child'] && $this->row !== 1
        || !$query->pseudo['first-child'] && $this->row === 1)
    ||  isset($query->pseudo['last-child'])
        && ($query->pseudo['last-child'] && $this->row !== $this->rowCount()
        || !$query->pseudo['last-child'] && $this->row === $this->rowCount())
    ||  isset($query->pseudo['only-child'])
        && ($query->pseudo['only-child'] && $this->rowCount() !== 1
        || !$query->pseudo['only-child'] && $this->rowCount() === 1)) {
      return FALSE;
    }

    if ((isset($query->pseudo['odd'])            && $type = 'odd')
    ||  (isset($query->pseudo['even'])           && $type = 'even')
    ||  (isset($query->pseudo['nth-child'])      && $type = 'nth-child')
    ||  (isset($query->pseudo['nth-last-child']) && $type = 'nth-last-child')) {
      if (($query->pseudo[$type] && !$this->nthChildEval($query))
      || (!$query->pseudo[$type] &&  $this->nthChildEval($query))) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Evaluate nth child pseudo selectors.
   *
   * @return Boolean
   */
  protected function nthChildEval($query) {

    // This preprocessing will only done once.
    if (empty($query->nth_child_eval)) {
      $args = NULL;
      if (isset($query->nth_child_args)) {
        $args = $query->nth_child_args;
      }
      if (!empty($args->nth_a) || !empty($args->nth_b)) {
        // :nth-child(-n+b)
        $a = !empty($args->nth_a) ? $args->nth_a === '-' ? -1 : (int) $args->nth_a : 0;
        $b = !empty($args->nth_b) ? (int) $args->nth_b : 0;
        // :nth-child(+an-b)
        if ($a > 0 && $b < 0) {
          $b += $a;
        }
      }
      elseif (isset($query->pseudo['odd'])
          || !empty($args->nth_keyword) && $args->nth_keyword === 'odd') {
        $a = 2; $b = 1;
      }
      elseif (isset($query->pseudo['even'])
          || !empty($args->nth_keyword) && $args->nth_keyword === 'even') {
        $a = 2; $b = 0;
      }

      $eval = new stdClass();
      $eval->a = $a;
      $eval->b = $b;
      $eval->from = $query->nth_child_args->type === 'nth-last-child' ? 'last' : 'first';
      $query->nth_child_eval[] = $eval;
    }

    // Evaluate nth children.
    foreach ($query->nth_child_eval as $eval) {
      $row = $this->row;
      if ($eval->from === 'last') {
        $row = $this->rowCount() - ($row - 1);
      }
      if ($eval->a === 0 && !($row === $eval->b)) {
        return FALSE;
      }
      // Compare positive or negative 'a' (before n).
      // Compare modulus offset of b and a to the modulus of current row and a.
      // Explanation of a,n and b: http://www.w3.org/TR/css3-selectors/#nth-child-pseudo
      elseif ($eval->a > 0 && !($row >= $eval->b && $eval->b % $eval->a === $row % $eval->a)) {
        return FALSE;
      }
      elseif ($eval->a < 0 && !($row <= $eval->b && $eval->b % $eval->a === $row % $eval->a)) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Match relationships.
   */
  protected function matchRelations($query, $prev) {
    // This is processed after the first match.
    // Normal descendents have no business here.
    if (!empty($query->descendent)) {
      // Ensure the relationship between the previous match and the current is correct.
      if (!empty($query->immediate_descendent) && !isset($prev[$this->depth-1]) && $r !== 1
      ||  !empty($query->adjacent_sibling)     && !isset($prev[$this->depth][$this->row-1])
      ||  !empty($query->general_sibling)      && !isset($prev[$this->depth])) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Checks to see if the current element has content.
   *
   * @return Boolean
   */
  protected function hasContent() {
    if ($content = $this->current()) {
      if (!empty($content['#printed'])) {
        return !empty($content['#children']);
      }
      return TRUE;
    }
    return FALSE;
  }

  /**
   * @return
   *  Total number of rows excluding attributes.
   */
  protected function rowCount() {
    if (!$this->row_count) {
      foreach ($this->parent->current() as $index => $current) {
        if ($index[0] !== '#') {
          ++$this->row_count;
        }
      }
    }
    return $this->row_count;
  }

  /**
   * Get attribute value.
   *
   * @param $attribute
   *  Attribute of the current element. Do not include the hash '#' prefix.
   */
  protected function getAttribute($attribute) {
    $current = $this->current();
    return isset($current['#' . $attribute]) ? $current['#' . $attribute] : NULL;
  }

  /**
   * Get the attribute id.
   */
  protected function getAttributeId() {
    $id = NULL;
    if (($attribute = $this->getAttribute('attributes')) && isset($attribute['id'])) {
      $id = $attribute['id'];
    }
    return $id;
  }

  /**
   * Get the attribute id.
   */
  protected function getAttributeClass() {
    $class = array();
    if (($attribute = $this->getAttribute('attributes')) && isset($attribute['class'])) {
      $class = $attribute['class'];
    }
    return $class;
  }

  /**
   * @return
   *  A unique query string for the current search.
   */
  public function queryId() {
    return hash('sha256', serialize($this->query));
  }

  /**
   * @return
   *  An array of paths leading to the found results.
   */
  public function getResults() {
    return $this->found;
  }

  /**
   * Checks if the current element contains children. It must also pass the
   * acceptChildren() method.
   *
   * @return
   *  Whether the current element has children.
   */
  public function hasChildren() {
    return $this->getInnerIterator()->hasChildren() && $this->acceptChildren();
  }

  /**
   * Gets called when hasChildren() returns TRUE.
   *
   * @return
   *  An iterator for the elements children in the same class as $this.
   */
  public function getChildren() {
    $child = new $this($this->getInnerIterator()->getChildren());
    // Carry over state.
    $child->parent     = $this;
    $child->depth      = $this->depth + 1;
    $child->path_index = $this->path_index;
    $child->query      = $this->query;
    $child->q          = $this->q;
    $child->matches    = $this->matches;
    $child->found      =&$this->found;
    // Increment if $this or this transitioning parent had a match.
    if (isset($this->matches[$this->depth][$this->row])
    && !$this->query[$this->q]->final) {
      ++$child->q;
    }

    return $child;
  }

}
