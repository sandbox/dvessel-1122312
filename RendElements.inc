<?php

/**
 * @file    RendElements.inc
 * @brief   Extension of ArrayObject, made for Drupal Render Arrays.
 * @author  Joon Park (joon@dvessel.com)
 */

/**
 * Created for Drupal Render Arrays.
 *
 * ArrayObject implements IteratorAggregate, ArrayAccess, Countable.
 */
class RendElements extends ArrayObject {

  public $root;
  protected $query_ref;
  protected $max_depth;

  /**
   * @param $array
   *  The render array.
   */
  public function __construct(array $array = array()) {
    $this->root = &$array;
    parent::__construct(&$array);
  }

  /**
   * Sets the maximum depth when executing a search. The lower it is, the
   * faster it might perform but with less chances for results.
   *
   * @param $depth
   *  1 is the lowest value for it to register. 0 or FALSE will remove the max
   *  depth.
   */
  public function setMaxDepth($depth) {
    $this->max_depth = $depth ? (int) $depth : NULL;
  }

  /**
   * Passes render method call to the core drupal_render function and prints
   * out the results.
   *
   * @see http://api.drupal.org/api/search/7/render
   * @see http://api.drupal.org/api/search/7/drupal_render
   */
  public function render($search = '') {
    foreach ($this->executeSearch($search)->elements as &$element) {
      timer_start('RendElements::render');
      show($element);
      print drupal_render($element);
      timer_stop('RendElements::render');
    }
  }

  /**
   * Passes hide method call to the core hide function.
   *
   * @see http://api.drupal.org/api/search/7/hide
   */
  public function hide($search = '') {
    foreach ($this->executeSearch($search)->elements as &$element) {
      hide($element);
    }
  }

  /**
   * Passes show method call to the core show function.
   *
   * @see http://api.drupal.org/api/search/7/show
   */
  public function show($search = '') {
    foreach ($this->executeSearch($search)->elements as &$element) {
      show($element);
    }
  }

  /**
   * Checks for non-empty results.
   */
  public function has($search = '') {
    foreach ($this->executeSearch($search)->elements as $result) {
      $element = !empty($result['#printed']) && isset($result['#children']) ? $result['#children'] : $result;
      if (!empty($element)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Checks to see if there are any results for a given search string.
   */
  public function exists($search = '') {
    return $this->executeSearch($search)->count !== 0;
  }

  /**
   * Override from ArrayObject to keep the array in sync.
   * 
   * This is not particularly useful in this context but if it is used,
   * provide expected results.
   *
   * @param $array
   *  new array
   */
  public function exchangeArray(array $array) {
    $this->root = &$array;
    parent::exchangeArray(&$array);
  }

  /**
   * Executes a search and returns all the results.
   * 
   * @param $search
   *  The search string or sets of search strings.
   * @return
   *  Sub-elements referenced from the main array, the count of items found
   *  and the path to each element.
   */
  public function executeSearch($search) {

    $return = new stdClass();
    $return->elements = array();
    $return->count   = 0;

    // When the search is empty, return root.
    if (trim($search) === '') {
      $return->elements['::root::'] = &$this->root;
      ++$return->count;
      return $return;
    }

    timer_start('RendElements::executeSearch');

    $it = new RecursiveIteratorIterator(
            new RendFilter(
              new RecursiveArrayIterator(array('::root::' => $this->root))));

    if (isset($this->max_depth)) {
      $it->setMaxDepth($this->max_depth);
    }

    $rend_filter = $it->getInnerIterator();
    for ($rend_filter->selection($search);
         $rend_filter->search();
         $rend_filter->searchNext()) {

      $qid = $rend_filter->queryId();

      // Build paths for the query if it doesn't exist.
      if (!isset($this->query_ref[$qid])) {
        $this->query_ref[$qid] = array();
        // Force trigger of internal filter. Takes the most CPU time.
        iterator_count($it);

        foreach ($rend_filter->getResults() as $paths) {
          foreach ($paths as $path) {
            $path === '::root::' ? $element = &$this->root : $element = &$element[$path];
          }
          $this->query_ref[$qid][implode(' > ', $paths)] = &$element;;
        }
      }
      // Reference to return element.
      $return->elements = &$this->query_ref[$qid];
      $return->count    = count($return->elements);
    }

    timer_stop('RendElements::executeSearch');

    return $return;
  }
}
